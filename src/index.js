import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Timer } from './Timer';
import Provider from './app/Provider';
import { createStore } from './app/stores/store';
import * as serviceWorker from './serviceWorker';
import { reducer } from './app/reducers/Reducer';


// ReactDOM.render(<App />, document.getElementById('root'));
let initialState = { 
    interval: 1
  }  

const store = createStore(reducer, initialState);

// init
ReactDOM.render(
    <Provider store={store}>
        <Timer />
    </Provider>,
    document.getElementById('root')
)


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();