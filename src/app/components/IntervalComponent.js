import React from 'react';
import './styles/styles.css';

export default class IntervalComponent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      interval: 1,
    };
    this.setStateInterval = this.setStateInterval.bind(this);
  }

  setStateInterval = (value) => {
    this.setState({ interval: this.state.interval + value });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.interval !== this.state.interval) {
      this.props.changeInterval(this.state.interval); //
    }
  }

  render() {

    return (
      <div>
        <span>Интервал обновления секундомера: {this.state.interval} сек.</span>
        <span>
          <button 
            className="timer-button"
            onClick={() => this.setStateInterval(-1)}>-</button>
          <button 
            className="timer-button"
            onClick={() => this.setStateInterval(1)}>+</button>
        </span>
      </div>
    )
  }
}