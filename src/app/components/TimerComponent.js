import React, { Component } from 'react';
import { Interval } from './Interval';
import './styles/styles.css';

export default class TimerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
    }
    this.handleStart = this.handleStart.bind(this);
    this.handleStop = this.handleStop.bind(this);
    this.tick = this.tick.bind(this);
  }

  tick() {
    this.setState(state => ({
      currentTime: state.currentTime + 1
    }));
    console.log('currentTime ' + this.state.currentTime);
    console.log('currentInterval ' + this.props.currentInterval);
  }

  handleStart() {
    if (this.state.currentTime === 0) {
      this.handleInterval = setInterval(() =>
        this.tick(),
        (this.props.currentInterval) * 1000);
    } else {
      this.setState({
        currentTime: 0
      });
    }
  }

  handleStop() {
    clearInterval(this.handleInterval);
    this.setState({
      currentTime: 0
    });
  }

  componentWillUnmount() {
    clearInterval(this.handleInterval);
  }


  render() {


    return (
      <div className="timer-body">
        <Interval />
        <div className="timer-view">
          Секундомер: {this.state.currentTime} сек.
          </div>
        <div>
          <button
            onClick={this.handleStart}
            className="timer-button">Старт
          </button>
          <button
            onClick={this.handleStop}
            className="timer-button">Стоп
            </button>
        </div>
      </div>
    )
  }
}