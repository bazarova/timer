import { changeInterval } from '../actions/actions';
import IntervalComponent from '../components/IntervalComponent';
import { connect } from '../Connect';


export const Interval = connect(
  state => ({
    currentInterval: state.interval,
  }),
  dispatch => ({
    changeInterval: value => dispatch(changeInterval(value)),
  })
)(IntervalComponent);
