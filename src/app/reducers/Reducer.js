  import { CHANGE_INTERVAL } from '../actions/actions';

  // reducers
  export const reducer = (state, action) => {
    switch(action.type) {
      case CHANGE_INTERVAL:
      return { ...state, interval: action.payload}
      default:
        return state
    }
  }