Для лучшего понимания разбила весь код на папки и компоненты:<br>

src<br>
&nbsp;&nbsp;index.js<br>
&nbsp;&nbsp;Timer.js<br>
&nbsp;&nbsp;папка app<br>
&nbsp;&nbsp;&nbsp;&nbsp;actions<br>
&nbsp;&nbsp;&nbsp;&nbsp;components<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;styles<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interval.js<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IntervalComponent.js<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TimerComponent.js<br>
&nbsp;&nbsp;&nbsp;&nbsp;reducers<br>
&nbsp;&nbsp;&nbsp;&nbsp;stores<br>
&nbsp;&nbsp;&nbsp;&nbsp;Connect.js<br>
&nbsp;&nbsp;&nbsp;&nbsp;Provider.js<br>

Основная проблема была в том, что не был задан начальный интервал<br>
<br>
в createStore добавляем и прокидываем в параметры interval, чтобы в него добавить action.payload, равный value<br>
<br>
поэтому в Timer.js в значение currentInterval заносим не весь объект state, а именно state.interval<br>

<br>
Основные изменения нужны были в компонентах TimerComponent, IntervalComponent:<br>
<br>
TimerComponent:<br>
setTimeout заменен на setInterval, чтобы таймер вызывался с определенным интервалом плюс это проще, чем setTimeout и цикл;<br>
начальный state добавила в конструктор, т.к. мне показалось это более правильным, также привязываем через bind this,<br>

при нажатии на Старт запускается функция tick() (для удобства вынесла отдельно) с заданным интервалом, который берется из props (который через коннект берет changeInterval), а время из state.<br>
чтобы при повторном нажатии на Старт значение сбросилось, добавляется условие, при котором если currentTime не 0, то обновляем состояние<br>
<br>
при нажатии на Стоп добавляем clearInterval<br>


в Interval.js также прокидываем state.interval<br>


IntervalComponent:<br>
добавляем в компонент состояние для интервала, т.к. состояние нужно менять через setState, добавляем метод setStateInterval,добавляем componentDidUpdate(prevProps, prevState), он нужен, чтобы время менялось с заданным интервалом<br>
в changeInterval прокидываем текущее состояние, если оно не равно предыдущему состоянию<br>
<br>
в Reducer.js<br>
return state += action.payload, лучше вернуть так - добавляем все из старого state и интервал со значением action.payload, который изменился и по умолчанию делаем state<br>